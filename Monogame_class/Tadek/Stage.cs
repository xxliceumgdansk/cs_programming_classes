using System;

namespace PongGame
{
	public class Stage
	{
		const int DefaultStageWidth = 705;
		const int DefaultStageHeight = 385;
		public Stage ()
		{
			Width = DefaultStageWidth;
			Height = DefaultStageHeight;
		}
		public void Interact (Ball ball) 
		{
			var position = ball.Position;

			var speed = ball.Speed;

			int xBoundaryPassed = (int)Math.Max(0, Math.Min(position.X, Width));

			if (xBoundaryPassed != position.X) {

				var diffX = Math.Abs (2 * xBoundaryPassed - position.X);

				position.X = (float)diffX;

				speed.X = -speed.X;

			}

			int yBoundaryPassed = (int)Math.Max(0, Math.Min(position.Y, Height));

			if (yBoundaryPassed != position.Y) {

				var diffY = Math.Abs (2 * yBoundaryPassed - position.Y);

				position.Y = (float)diffY;

				speed.Y = -speed.Y;

			}

			ball.Position = position;

			ball.Speed = speed;

		}
		public int Width {get; set;}
		public int Height {get; set;}
	}
}

