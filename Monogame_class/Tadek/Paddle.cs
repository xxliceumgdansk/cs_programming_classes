﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;


namespace PongGame
{
    public class Paddle
    {
        private Texture2D texture;
        public Vector2 Position;
        public Vector2 Speed;
        public Stage _stage;
        public Paddle (Texture2D ballTexture, Stage stage)
		{
			texture = ballTexture;
			_stage = stage;
            Position.X = 350;
            Position.Y = 445;
            Speed.X = 4;
		}
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, this.Position, new Rectangle(0, 0, 76, 35), Color.White);
        }
        public void Move(PaddleDirection paddleDirection)
        {
            if(paddleDirection == PaddleDirection.Left && Position.X > 0)
            {
                Position -= Speed;
            }
            if(paddleDirection == PaddleDirection.Right && Position.X < 725)
            {
                Position += Speed;
            }
        }
        public void Grab(Ball ball)
        {
            ball.Position.X = Position.X - 12;
            ball.Position.Y = 345;
            ball.Speed -= ball.Speed;
        }
        public void Throw(Ball ball)
        {
            ball.Speed = new Vector2(5, -5);
        }
    }
}
