using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;

namespace PongGame
{
	public class Ball
	{
		private Texture2D texture;
        public Vector2 Position;
		public Vector2 Speed { get; set;}
		public Stage _stage;
		public Ball (Texture2D ballTexture, Stage stage)
		{
			texture = ballTexture;
			_stage = stage;
            Position.X = 0;
            Position.Y = 0; 
		}

		public void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Draw(this.texture, this.Position, new Rectangle(0, 0, 100, 100), Color.White);
		}
		public void Fly()
		{
			Position += Speed;
			_stage.Interact(this);
		}
	}
}

