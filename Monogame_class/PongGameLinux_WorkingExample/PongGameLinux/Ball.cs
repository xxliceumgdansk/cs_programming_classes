﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PongGameLinux
{
    public class Ball
    {
        private readonly BlockMap _map;
        private Vector2 _speed;

        public Ball(BlockMap map)
        {
            _map = map;
        }

        public Vector2 Position { get; set; }

        public bool OutOfMap
        {
            get { return Position.Y > _map.Height; }
        }

        public void Throw(Vector2 speed)
        {
            _speed = speed;
        }

        public void Fly()
        {
            var position = Position;
            position += _speed;

            // Cover left boundary of paddle movement.
            if (position.X >= _map.Width)
            {
                var diffX = Math.Abs(_map.Width - 2 * position.X);
                position.X = (float) diffX;

                _speed.X = -1*_speed.X;
            }

            // Cover right boundary of paddle movement.
            if (position.X < 0)
            {
                position.X = Math.Abs(position.X);
                _speed.X = -1*_speed.X;
            }

            if (position.Y < 0)
            {
                position.Y = Math.Abs(position.Y);
                _speed.Y = -1*_speed.Y;
            }

            Position = position;
        }

        public void Stop()
        {
            _speed = new Vector2();
        }

        public void Reflect()
        {
            _speed.Y = -1*_speed.Y;
        }
    }
}