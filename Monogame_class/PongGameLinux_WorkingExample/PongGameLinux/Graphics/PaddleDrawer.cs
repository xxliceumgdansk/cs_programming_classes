﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace PongGameLinux.Graphics
{
    public class PaddleDrawer : IDrawer
    {
        private readonly Paddle _paddle;
        private readonly Texture2D paddleTexture;

        public PaddleDrawer(Paddle paddle, GraphicsDevice graphicsDevice)
        {
            _paddle = paddle;

            paddleTexture = new Texture2D(graphicsDevice, 1, 1);
            paddleTexture.SetData(new[] {Color.White});
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(paddleTexture, _paddle.Position, new Rectangle(0, 0, 60, 10), Color.Green);
        }
    }
}