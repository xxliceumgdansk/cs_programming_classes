﻿using Microsoft.Xna.Framework.Graphics;

namespace PongGameLinux.Graphics
{
    public interface IDrawer
    {
        void Draw(SpriteBatch spriteBatch);
    }
}