﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace PongGameLinux.Graphics
{
    public class BallDrawer : IDrawer
    {
        private Ball _ball;
        private Texture2D _ballTexture;

        public BallDrawer(Ball ball, GraphicsDevice graphicsDevice, ContentManager content)
        {
            _ball = ball;
            _ballTexture = content.Load<Texture2D>("Ball");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_ballTexture, _ball.Position, new Rectangle(0,0, 10,10), Color.Yellow);
        }
    }
}