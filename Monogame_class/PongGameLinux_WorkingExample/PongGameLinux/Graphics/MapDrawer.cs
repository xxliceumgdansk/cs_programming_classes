﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PongGameLinux.Graphics
{
    public class MapDrawer : IDrawer
    {
        private readonly BlockMap _map;
        private Texture2D background;

        public MapDrawer(BlockMap map, GraphicsDevice graphicsDevice)
        {
            _map = map;
            // Create a 1px square rectangle texture that will be scaled to the
            // desired size and tinted the desired color at draw time
            background = new Texture2D(graphicsDevice, 1, 1);
            background.SetData(new[] { Color.White });
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, new Rectangle(0,0, 600, 400), Color.Yellow);
        }
    }
}
