﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace PongGameLinux.Graphics
{
    public class ScoreDrawer : IDrawer
    {
        private readonly ScoreCounter _scoreCounter;
        private readonly SpriteFont _spriteFont;

        public ScoreDrawer(ScoreCounter scoreCounter, GraphicsDevice graphicsDevice, ContentManager content)
        {
            _scoreCounter = scoreCounter;
            _spriteFont = content.Load<SpriteFont>("Arial");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var score in _scoreCounter.Scores)
            {
                
                spriteBatch.DrawString(_spriteFont, string.Format("Score {0}", score.Value), new Vector2(20, 20),
                    Color.Blue);
            }
        }
    }
}