using System.Collections.Generic;
using PongGameLinux.Graphics;

namespace PongGameLinux
{
    public class GraphicsHandler
    {
        private readonly IList<IDrawer> _drawers;

        public GraphicsHandler(IList<IDrawer> drawers)
        {
            this._drawers = drawers;
        }

        public IList<IDrawer> Drawers
        {
            get { return _drawers; }
        }
    }
}