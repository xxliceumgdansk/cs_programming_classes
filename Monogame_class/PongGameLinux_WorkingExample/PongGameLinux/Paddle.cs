﻿using Microsoft.Xna.Framework;

namespace PongGameLinux
{
    public class Paddle
    {
        private const double PaddleWidth = 60;
        private readonly BlockMap _blockMap;
        private readonly Vector2 _paddleSpeed;
        private Ball _ball;
        private Vector2 _position;

        public Paddle(BlockMap blockMap)
        {
            _blockMap = blockMap;
            _paddleSpeed = new Vector2(10, 0);
            Position = new Vector2(blockMap.Height - 10);
        }

        public Vector2 Position
        {
            get { return _position; }
            private set { _position = value; }
        }

        public void ThrowBall()
        {
            if (_ball != null)
            {
                _ball.Throw(new Vector2(10, -10));
                _ball = null;
            }
        }

        public void GrabBall(Ball ball)
        {
            ball.Stop();
            _ball = ball;
        }

        public void Move(PaddleDirection paddleDirection)
        {
            switch (paddleDirection)
            {
                case PaddleDirection.Left:
                    _position -= _paddleSpeed;
                    if (_position.X < 0)
                    {
                        _position.X = 0;
                    }
                    break;
                case PaddleDirection.Right:
                    _position += _paddleSpeed;
                    if (_position.X > _blockMap.Width - PaddleWidth)
                    {
                        _position.X = (float) (_blockMap.Width - PaddleWidth);
                    }
                    break;
            }

            if (_ball != null)
            {
                _ball.Position = Position + new Vector2((float) (PaddleWidth/2), -10);
            }
        }

        public bool ReflectBall(Ball ball)
        {
            if (ball.Position.Y >= Position.Y && ball.Position.X > Position.X && ball.Position.X < Position.X + 60)
            {
                ball.Reflect();

                return true;
            }

            return false;
        }
    }
}