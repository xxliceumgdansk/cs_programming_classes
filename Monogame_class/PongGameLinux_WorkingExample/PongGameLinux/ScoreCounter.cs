using System.Collections.Generic;

namespace PongGameLinux
{
    public class ScoreCounter
    {
        private readonly Dictionary<Paddle, int> _scores;

        public ScoreCounter()
        {
            _scores = new Dictionary<Paddle, int>();
        }

        public void RegisterPoint(Paddle paddle)
        {
            if (!_scores.ContainsKey(paddle))
            {
                _scores.Add(paddle, 0);
            }

            _scores[paddle] ++;
        }

        public Dictionary<Paddle, int> Scores
        {
            get { return _scores; }
        }

        public void Clear()
        {
            _scores.Clear();
        }
    }
}