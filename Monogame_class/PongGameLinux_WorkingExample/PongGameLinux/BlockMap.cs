﻿namespace PongGameLinux
{
    public class BlockMap
    {
        public BlockMap(int[][] map)
        {
            Map = map;
            Width = 600;
            Height = 400;
        }

        public int[][] Map { get; private set; }

        public float Height { get; set; }

        public double Width { get; set; }

        public void Destroy(Ball ball)
        {
        }

        public bool IsCollision(Ball ball)
        {
            // This is about to turn to breakout.
            return true;
        }
    }
}