﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PongGameLinux.Graphics;

namespace PongGameLinux
{
    public class PongGame
    {
        private readonly Ball _ball;
        private readonly Paddle _paddle;
        private readonly ScoreCounter _scoreCounter;
        private readonly GraphicsHandler _graphicsHandler;

        public PongGame(GraphicsDevice graphicsDevice, ContentManager content)
        {
            var blockMap = new BlockMap(new int[][] {});
            _paddle = new Paddle(blockMap);
            _ball = new Ball(blockMap);
            _paddle.GrabBall(_ball);
            _scoreCounter = new ScoreCounter();

            var drawers = new List<IDrawer>
            {
                new PaddleDrawer(_paddle, graphicsDevice),
                new BallDrawer(_ball, graphicsDevice, content),
                new ScoreDrawer(_scoreCounter, graphicsDevice, content)
            };
            _graphicsHandler = new GraphicsHandler(drawers);
        }

        public GraphicsHandler GraphicsHandler
        {
            get { return _graphicsHandler; }
        }

        public GameStatus Proceed(KeyboardState keyboardState)
        {
            if (keyboardState.IsKeyDown(Keys.Space))
            {
                _paddle.ThrowBall();
            }

            var direction = GetPaddleDirection(keyboardState);

            _paddle.Move(direction);

            if (_paddle.ReflectBall(_ball))
            {
                _scoreCounter.RegisterPoint(_paddle);
            }
            
            _ball.Fly();

            if (_ball.OutOfMap)
            {
                _paddle.GrabBall(_ball);
                _scoreCounter.Clear();
            }

            return GameStatus.InProgress;
        }

        private static PaddleDirection GetPaddleDirection(KeyboardState keyboardState)
        {
            var direction = PaddleDirection.None;

            if (keyboardState.IsKeyDown(Keys.Left))
            {
                direction = PaddleDirection.Left;
            }

            if (keyboardState.IsKeyDown(Keys.Right))
            {
                direction = PaddleDirection.Right;
            }
            return direction;
        }
    }
}