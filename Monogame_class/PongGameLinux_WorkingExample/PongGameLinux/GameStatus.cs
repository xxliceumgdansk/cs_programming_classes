﻿namespace PongGameLinux
{
    public enum GameStatus
    {
        Won,
        Lost,
        InProgress
    }
}